from jira import JIRA
from jira.exceptions import JIRAError
import os
from dotenv import load_dotenv
load_dotenv()

server = os.getenv("SERVER_URL")
username = os.getenv("JIRA_USERNAME")
password = os.getenv("PASSWORD")


class UserDoesNotExist(Exception):
    pass


class JiraHandler:
    UserDoesNotExist = UserDoesNotExist

    def __init__(self):
        self.jira = JIRA(
            basic_auth=(username, password),
            options={"server": server}
        )
        self.renamed_counter = 0
        self.not_renamed_counter = 0
        self.user_not_renamed = []

    def rename_user(self, old_user, new_user):
        try:
            self.jira.rename_user(old_user, new_user)
            self.renamed_counter += 1
        except JIRAError:
            self.not_renamed_counter += 1
            self.user_not_renamed.append(old_user)
            raise UserDoesNotExist

    def update_status(self, username, status):
        self.jira.user(username).update(active=status)
