## Rename users
This is a module that allows you to rename username and status. Using Python version 3.6 or higher.

## Environment  
  
### Install Virtualenv


Check to see if your Python installation has pip. Enter the following in your terminal:

1) `$ pip -h`

You should expect to receive a system response indicating the pip version. If no pip is discovered, install it as described in the Installation Instructions. See to [Install pip](https://pip.pypa.io/en/stable/installing/).

2) To install virtualenv via pip you only have to execute this command:

`$ pip install virtualenv`

For more information: [Install virtualenv](https://virtualenv.pypa.io/en/latest/installation.html).

### Create virtual environment

1) To create a virtual environment, you must specify a path. For example to create one in the local directory called ‘mypython’, type the following:

`$ virtualenv mypython`

2) You can activate the python environment by running the following command:

**Mac Os / Linux**

`$ source mypython/bin/activate `

**Windows**

`$ mypthon\Scripts\activate`

You should see the name of your virtual environment in brackets on your terminal line e.g. (mypython).

For more detailed information, see the offical virtualenv documentation
 [Documentation](https://virtualenv.pypa.io/en/latest/user_guide.html).


The next steps are:

3) Clone [rename-username project](https://bitbucket.org/joaquindefrancesca/rename-username/src/master/) (this will download the project to your computer)


4) Inside the project install the necessary packages:
`$ pip install -r requirements.txt`

## Run the script
You must put the excel file inside the **rename-username** folder and then
you only have to execute this command:

`$ python rename_usernames.py` if the file name is usernames.xlsx.

or

`$ python rename_usernames.py --filename=EXCEL-FILE-NAME`

Note that **EXCEL-FILE-NAME** you must change it by the file name.


## Enviroment variables

To make this module works you'll need the `.env` file in order to have Jira credentials (username, password, JIRA instance URL). Otherwise, it will not work.
This file will go inside the rename-username folder. Please let **Joaquin de Francesca** know, and he will provide you that file.