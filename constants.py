# Excel name
DEFAULT_FILENAME = "usernames.xlsx"

# User status
STATUS = {'active': True, 'inactive': False}

# Program status
FILE_NOT_FOUND = "File not found"
OK = "Renamed finished."
