import pandas as pd
import argparse
from jira_handle import JiraHandler
from constants import *

jira = JiraHandler()


def show_message(status):
    msg = "{} renamed {} not renamed".format(
        jira.renamed_counter, jira.not_renamed_counter)

    print(msg, "\n")

    if (len(jira.user_not_renamed) > 0):
        print("Users not renamed:")
        for user in jira.user_not_renamed:
            print(user)
        print("\n")

    print(status)


def rename_user(filepath):
    try:
        table = pd.read_excel(filepath)
    except FileNotFoundError:
        return FILE_NOT_FOUND

    old_username = table.columns[0]
    new_username = table.columns[1]
    status = table.columns[2]

    for _, row in table.iterrows():
        if pd.isna(row[old_username]):
            continue
        try:
            jira.rename_user(row[old_username], row[new_username])
            if pd.notna(row[status]):
                try:
                    jira.update_status(row[new_username], STATUS[row[status]])
                except KeyError:
                    pass
        except jira.UserDoesNotExist:
            pass

    return OK


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--filename',
                        help='Type the name of the excel file.',
                        default=DEFAULT_FILENAME)
    args = parser.parse_args()

    status = rename_user(args.filename)
    show_message(status)


if __name__ == "__main__":
    main()
